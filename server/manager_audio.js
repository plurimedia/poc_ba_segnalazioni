require("dotenv-flow").config();
const mongoose = require("mongoose");
var s3 = require("@auth0/s3");
var path = require("path");
var dest = path.join(process.cwd(), ".tmp", "converted.mp3");
var ffmpeg = require("ffmpeg");
var cuid = require("cuid");
var AWS = require("aws-sdk");

// salvataggio rilevamento
exports.saveAudio = function(req, res) {
  var client = s3.createClient({
    s3Options: {
      accessKeyId: process.env.AWS_ACCESS_KEY_ID,
      secretAccessKey: process.env.AWS_SECRET_KEY,
      region: process.env.AWS_REGION,
      signatureVersion: "v4"
    }
  });
  var raw = new ffmpeg(req.files.audio.path);
  raw.then(audio => {
    audio.fnExtractSoundToMP3(dest, function(err) {
      if (err) {
        res.status(500).send(err);
      } else {
        var id = cuid();
        var filename = "segnalazioni_audio/" + id + ".mp3";
        console.log("filename", filename);
        var params = {
          localFile: dest,
          s3Params: {
            ContentType: "audio/mpeg",
            Bucket: process.env.AWS_BUCKET,
            Key: filename
          }
        };
        var uploader = client.uploadFile(params);
        uploader.on("error", function(err) {
          console.error("unable to upload:", err.stack);
        });
        uploader.on("progress", function() {
          console.log(
            "progress",
            uploader.progressMd5Amount,
            uploader.progressAmount,
            uploader.progressTotal
          );
        });
        uploader.on("end", function() {
          let url =
            "https://" +
            process.env.AWS_BUCKET +
            ".s3." +
            process.env.AWS_REGION +
            ".amazonaws.com/" +
            filename;
          console.log("url audio", url);

          var jobName = cuid();
          AWS.config.update({
            accessKeyId: process.env.AWS_ACCESS_KEY_ID,
            secretAccessKey: process.env.AWS_SECRET_KEY,
            signatureVersion: "v4",
            region: process.env.AWS_REGION
          });
          var transcribeService = new AWS.TranscribeService();
          var params = {
            LanguageCode: "it-IT",
            Media: {
              MediaFileUri: url
            },
            TranscriptionJobName: jobName,
            OutputBucketName: process.env.AWS_BUCKET
          };

          console.log("invio transcribe");
          var startTranscriptionJobPromise = transcribeService
            .startTranscriptionJob(params)
            .promise();

          startTranscriptionJobPromise
            .then(resp => {
              let audiojson = {};
              audiojson.url = url;
              audiojson.transcriptionJobName =
                resp.TranscriptionJob.TranscriptionJobName;
              console.log("audio json", audiojson);
              res.status(200).send(audiojson);
            })
            .catch(err => res.status(500).send(err));

          //res.status(200).send(url);
        });
      }
    });
  });
};
