require("dotenv-flow").config();
const express = require("express");
const helmet = require("helmet");
const mongoose = require("mongoose");
const cors = require("cors");
const compression = require("compression");

const path = require("path");

const app = express();
app.use(express.json()); //body-parser
app.use(helmet()); //protegge da attacchi base
app.use(compression()); //riduce il peso del payload
app.use(express.static(path.join(process.cwd(), "dist"))); //importante per deploy

const multipart = require("connect-multiparty"); // per uplaod files

const managerAudio = require("./server/manager_audio");

/* mongoose.Promise = global.Promise;
var mongoOptions = { useNewUrlParser: true };

mongoose.connect(process.env.MONGOURL, mongoOptions, err => {
  if (err) throw err;

  console.info("### Connesso a: ", process.env.MONGOURL);
  console.info("### Ambiente: ", process.env);
});
 */

app.use(express.static(".tmp"));

app.use(
  multipart({
    uploadDir: ".tmp"
  })
);

// cors origins (permette di alle api di essere chiamate da questi url)
var whitelist = ["http://localhost:3001", "https://ba-segnalazioni-poc.herokuapp.com", "https://ba-backend-segnalazioni-poc.herokuapp.com"],
  corsOptions = {
    origin: function(origin, callback) {
      var originIsWhitelisted = whitelist.indexOf(origin) !== -1;
      callback(null, originIsWhitelisted);
    }
  };

app.use(cors()); //applica le cors policy

// ROUTES
app.post("/api/audio/upload", managerAudio.saveAudio);

var port = process.env.PORT || 3001;
app.listen(port, () => {
  console.info("### Server in ascolto sulla porta: ", port);
});
