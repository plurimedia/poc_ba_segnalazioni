import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import "./registerServiceWorker";
import vuetify from "./plugins/vuetify";
import "material-design-icons-iconfont/dist/material-design-icons.css";
import VueRecord from "@codekraft-studio/vue-record";

import * as VueGoogleMaps from "vue2-google-maps";

Vue.use(VueRecord);

Vue.use(VueGoogleMaps, {
  load: {
    key: "AIzaSyDxAhHbMqkNUY6pjPcFRKolJn8RYYQg0Jc",
    libraries: "places" // This is required if you use the Autocomplete plugin
  },
  installComponents: true
});

Vue.config.productionTip = false;

Vue.prototype.$casetta = "prova"; // dichiarazione di costante globale

new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount("#app");
