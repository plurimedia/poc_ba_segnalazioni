import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../components/Home.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: Home
  },
  {
    path: "/posizione",
    name: "posizione",
    component: () => import("../components/Posizione.vue")
  },
  {
    path: "/segnalazione",
    name: "segnalazione",
    component: () => import("../components/Segnalazione.vue")
  },
  {
    path: "/success",
    name: "success",
    component: () => import("../views/SuccessMessage.vue")
  },
  {
    path: "/error",
    name: "error",
    component: () => import("../views/ErrorMessage.vue")
  }
];

const router = new VueRouter({
  routes
});

export default router;
