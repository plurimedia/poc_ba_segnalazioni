FROM centos:7
USER root

# environment pre-requirements (nodejs 12 repo)
RUN curl -sL https://rpm.nodesource.com/setup_12.x | bash -

# install yum needed packages
RUN yum -y install nodejs git wget && yum clean all -y

# pull from bitbucket repository
RUN git clone https://bitbucket.org/plurimedia/poc_ba_segnalazioni.git /opt/app-root

# creating non-root user and setting its permissions
RUN useradd nodejs && \
    chown -R nodejs:nodejs /opt && \
    chmod g+s /opt

# Install epel-release
RUN yum install -y epel-release && \
    rpm -v --import http://li.nux.ro/download/nux/RPM-GPG-KEY-nux.ro && \
    rpm -Uvh http://li.nux.ro/download/nux/dextop/el7/x86_64/nux-dextop-release-0-5.el7.nux.noarch.rpm

# Install ffmpeg ffmpeg-devel
RUN yum install -y ffmpeg ffmpeg-devel && \
    ffmpeg -version

# expose port
EXPOSE 8080

# switch to non-root user
USER nodejs

# set prefix to nodejs directory, no need to use sudo on -g flag
RUN mkdir /home/nodejs/.npm-global &&\
    npm config set prefix '/home/nodejs/.npm-global'

# npm install dependencies
RUN cd /opt/app-root && \
    npm install && \
    npm install -g @vue/cli && \
    npm install -g @vue/cli-service-global && \
    vue build src/App.vue && \
    npm run build

# create tmp directories
RUN mkdir /opt/app-root/src/.tmp || :

WORKDIR /opt/app-root
CMD node server.js

